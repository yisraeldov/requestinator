<?php

$request = $_REQUEST;

$filename = getRequestFilePath($_SERVER);
if (isset($request['inspect'])) {
    echo "inspecting $hash" . PHP_EOL ;
    echo file_get_contents($filename);
    return;
}

storeRequest($request);

function storeRequest(array $request)
{
    $filename = getRequestFilePath($_SERVER);
    file_put_contents($filename, json_encode($request, JSON_PRETTY_PRINT));
    echo('ok');
}

function getRequestFilePath(array $serverVars): string
{
    $parsedUrl = parse_url($serverVars['REQUEST_URI']);
    $hash = md5($parsedUrl['path']);
    $dataPath = sys_get_temp_dir() . DIRECTORY_SEPARATOR;
    return  $dataPath . $hash;
}
