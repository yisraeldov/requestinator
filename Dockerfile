# This file is a template, and might need editing before it works on your project.
FROM php:7.2-apache

# Customize any core extensions here
RUN apt-get update && apt-get install -y \
        git libmcrypt-dev libpq-dev libcurl4-gnutls-dev libicu-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev
RUN pecl install mcrypt-1.0.2 && docker-php-ext-enable mcrypt	
RUN docker-php-ext-install mbstring pdo_pgsql curl json intl gd xml zip bz2 opcache

RUN a2enmod rewrite
RUN a2enmod negotiation

RUN curl -sS https://getcomposer.org/installer | php

#COPY config/php.ini /usr/local/etc/php/
COPY ./ /var/www/

RUN  php composer.phar install -d /var/www/
