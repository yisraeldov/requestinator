<?php

namespace Tests\Features;

use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    private function getHostName(): string
    {
        return $_ENV['HOSTNAME'];
    }

    private function fetchLocalUrl(string $path):string
    {
        return file_get_contents("http://" . $this->getHostName() . $path);
    }

    public function testRequestOk(): void
    {
        $result = $this->fetchLocalUrl("/");
        $this->assertNotEmpty($result);
    }

    public function testsCanRandomPath(): void
    {
        $result = $this->fetchLocalUrl("/asdfasdfasdfasd");
        $this->assertNotEmpty($result);
    }

    public function testCanInspectPath(): void
    {
        $randValue = rand();
        $this->fetchLocalUrl("/atest?somedata=1234".$randValue);
        $result = $this->fetchLocalUrl("/atest?inspect=1");
        $this->assertStringContainsString("somedata", $result);
        $this->assertStringContainsString("1234".$randValue, $result);
    }
}
