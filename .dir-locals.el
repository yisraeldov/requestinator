;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((php-mode
  (projectile-project-test-cmd . "docker-compose exec app ./vendor/bin/phpunit")))
